import it.unimi.di.musica.controller.Handler;
import it.unimi.di.musica.model.Level;
import it.unimi.di.musica.view.Window;

public class Start {
    public static void main(String[] args){

        java.awt.EventQueue.invokeLater(new Runnable() {

            private Handler controller;
            private Level model;
            private Window view;

            @Override
            public void run() {
                model = new Level();
                view = new Window();
                model.addPropertyChangeListener(view);

                controller = new Handler(model, view);
                controller.init();
                view.addKeyListener(controller.getKeyListener());
                view.setFocusable(true);
            }
        });

    }
}
