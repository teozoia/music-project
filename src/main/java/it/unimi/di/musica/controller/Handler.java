package it.unimi.di.musica.controller;

import it.unimi.di.musica.model.*;
import it.unimi.di.musica.view.BlackKey;
import it.unimi.di.musica.view.WhiteKey;
import it.unimi.di.musica.view.Window;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;


public class Handler {
    private Level model;
    private Window view;

    private KeyListener keyListener;
    private MouseListener mouseListenerWhite;
    private MouseListener mouseListenerBlack;

    public Handler(Level model, Window view){
        this.model = model;
        this.view = view;

        keyListener = new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) { }

            @Override
            public void keyPressed(KeyEvent e) {
                switch(e.getKeyChar()){
                    // White keys
                    case 'a':
                        presedNote(Pitch.C, Accidental.NATURAL, 0, 0);
                        break;
                    case 's':
                        presedNote(Pitch.D, Accidental.NATURAL, 0, 1);
                        break;
                    case 'd':
                        presedNote(Pitch.E, Accidental.NATURAL, 0, 2);
                        break;
                    case 'f':
                        presedNote(Pitch.F, Accidental.NATURAL, 0, 3);
                        break;
                    case 'g':
                        presedNote(Pitch.G, Accidental.NATURAL, 0, 4);
                        break;
                    case 'h':
                        presedNote(Pitch.A, Accidental.NATURAL, 0, 5);
                        break;
                    case 'j':
                        presedNote(Pitch.B, Accidental.NATURAL, 0, 6);
                        break;

                    // Black keys
                    case 'w':
                        presedNote(Pitch.C, Accidental.SHARP, 1, 0);
                        break;
                    case 'e':
                        presedNote(Pitch.D, Accidental.SHARP, 1, 1);
                        break;
                    case 't':
                        presedNote(Pitch.F, Accidental.SHARP, 2, 0);
                        break;
                    case 'y':
                        presedNote(Pitch.G, Accidental.SHARP, 2, 1);
                        break;
                    case 'u':
                        presedNote(Pitch.A, Accidental.SHARP,2, 2);
                        break;
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                switch(e.getKeyChar()){
                    // White keys
                    case 'a':
                        releasedNote(0, 0);
                        break;
                    case 's':
                        releasedNote( 0, 1);
                        break;
                    case 'd':
                        releasedNote( 0, 2);
                        break;
                    case 'f':
                        releasedNote( 0, 3);
                        break;
                    case 'g':
                        releasedNote( 0, 4);
                        break;
                    case 'h':
                        releasedNote( 0, 5);
                        break;
                    case 'j':
                        releasedNote( 0, 6);
                        break;

                    // Black keys
                    case 'w':
                        releasedNote( 1, 0);
                        break;
                    case 'e':
                        releasedNote( 1, 1);
                        break;
                    case 't':
                        releasedNote(2, 0);
                        break;
                    case 'y':
                        releasedNote(2, 1);
                        break;
                    case 'u':
                        releasedNote(2, 2);
                        break;
                }
            }
        };

        mouseListenerWhite = new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {}

            @Override
            public void mousePressed(MouseEvent e) {
                e.getComponent().setBackground(Color.RED);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                e.getComponent().setBackground(Color.WHITE);
                WhiteKey k = (WhiteKey) e.getSource();
                switch(k.getNote()){
                    case "C":
                        clickedNote(Pitch.C, Accidental.NATURAL);
                        break;
                    case "D":
                        clickedNote(Pitch.D, Accidental.NATURAL);
                        break;
                    case "E":
                        clickedNote(Pitch.E, Accidental.NATURAL);
                        break;
                    case "F":
                        clickedNote(Pitch.F, Accidental.NATURAL);
                        break;
                    case "G":
                        clickedNote(Pitch.G, Accidental.NATURAL);
                        break;
                    case "A":
                        clickedNote(Pitch.A, Accidental.NATURAL);
                        break;
                    case "B":
                        clickedNote(Pitch.B, Accidental.NATURAL);
                        break;
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                ((JPanel)e.getComponent()).setBorder(new LineBorder(Color.RED, 2, true));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                ((JPanel)e.getComponent()).setBorder(new LineBorder(Color.DARK_GRAY, 2, true));
            }
        };

        mouseListenerBlack = new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {}

            @Override
            public void mousePressed(MouseEvent e) {
                e.getComponent().setBackground(Color.RED);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                e.getComponent().setBackground(Color.BLACK);
                BlackKey k = (BlackKey) e.getSource();
                switch(k.getNote()){
                    case "C#":
                        clickedNote(Pitch.C, Accidental.SHARP);
                        break;
                    case "D#":
                        clickedNote(Pitch.D, Accidental.SHARP);
                        break;
                    case "F#":
                        clickedNote(Pitch.F, Accidental.SHARP);
                        break;
                    case "G#":
                        clickedNote(Pitch.G, Accidental.SHARP);
                        break;
                    case "A#":
                        clickedNote(Pitch.A, Accidental.SHARP);
                        break;
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                ((JPanel)e.getComponent()).setBorder(new LineBorder(Color.RED, 2, true));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                ((JPanel)e.getComponent()).setBorder(new LineBorder(Color.DARK_GRAY, 2, true));
            }
        };

    }

    public void init(){
        model.init();
        addMouseListenerWhite();
        addMouseListenerBlack();
    }

    private void presedNote(Pitch p, Accidental a, int keyListCode, int index) {

        Note playedNote = null;
        try {
            playedNote = new Note(p, 4, a); // Stub octave = 4
            model.playNote(playedNote);
            view.pressedKey(keyListCode, index);
        } catch (NoteException e) {
            e.printStackTrace();
        }
    }

    private void clickedNote(Pitch p, Accidental a) {

        Note playedNote = null;
        try {
            playedNote = new Note(p, 4, a); // Stub octave = 4
            model.playNote(playedNote);
        } catch (NoteException e) {
            e.printStackTrace();
        }
    }

    private void releasedNote(int keyListCode, int index) {
        view.releasedKey(keyListCode, index);
    }

    public KeyListener getKeyListener() {
        return keyListener;
    }

    public void addMouseListenerWhite(){
        view.addMouseListenerWhite(mouseListenerWhite);
    }

    public void addMouseListenerBlack(){
        view.addMouseListenerBlack(mouseListenerBlack);
    }

}
