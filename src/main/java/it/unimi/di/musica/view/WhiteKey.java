package it.unimi.di.musica.view;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class WhiteKey extends Key {

    private static final int WIDTH = 80;
    private static final int HEIGHT = 200;

    private static final int DIMBINDLABEL = WIDTH / 4;

    private String keyBind;
    private String note;

    private JPanel labelPanel;
    private JLabel keyBindLabel;
    private JLabel noteLabel;

    public WhiteKey(String keyBind, String note){
        this.keyBind = keyBind;
        this.note = note;

        setLayout(new FlowLayout(FlowLayout.LEADING, (WIDTH / 2) - (DIMBINDLABEL / 2), 130));

        labelPanel = new JPanel();
        labelPanel.setLayout(new GridLayout(2,1, 0, 10));
        labelPanel.setBackground(new Color(255,255,255,0));

        noteLabel = new JLabel(note, SwingConstants.CENTER);
        noteLabel.setPreferredSize(new Dimension(DIMBINDLABEL, DIMBINDLABEL));
        noteLabel.setForeground(Color.DARK_GRAY);
        //noteLabel.setVisible(false);
        labelPanel.add(noteLabel);

        keyBindLabel = new JLabel(keyBind, SwingConstants.CENTER);
        keyBindLabel.setPreferredSize(new Dimension(DIMBINDLABEL, DIMBINDLABEL));
        keyBindLabel.setForeground(Color.WHITE);
        keyBindLabel.setBackground(Color.DARK_GRAY);
        keyBindLabel.setBorder(new LineBorder(Color.DARK_GRAY, 2, false));
        keyBindLabel.setOpaque(true);
        labelPanel.add(keyBindLabel);

        add(labelPanel);

        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        setBackground(Color.WHITE);
        setBorder(new LineBorder(Color.DARK_GRAY, 2, true));
        setFocusable(true);
    }

    @Override
    public void keyPressed() {
        setBackground(Color.RED);
        keyBindLabel.setBackground(Color.ORANGE);
    }

    @Override
    public void keyReleased() {
        setBackground(Color.WHITE);
        keyBindLabel.setBackground(Color.DARK_GRAY);
    }

    public String getNote(){
        return note;
    }
}
