package it.unimi.di.musica.view;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class BlackKey extends Key{

    private static final int WIDTH = 40;
    private static final int HEIGHT = 150;

    private static final int DIMBINDLABEL = WIDTH / 2;

    private String keyBind;
    private String note;

    private JPanel labelPanel;
    private JLabel keyBindLabel;
    private JLabel noteLabel;

    public BlackKey(String keyBind, String note){
        this.keyBind = keyBind;
        this.note = note;

        setLayout(new FlowLayout(FlowLayout.LEADING, (WIDTH / 2) - (DIMBINDLABEL / 2), 80));

        labelPanel = new JPanel();
        labelPanel.setLayout(new GridLayout(2,1, 0, 10));
        labelPanel.setBackground(new Color(255,255,255,0));

        noteLabel = new JLabel(note, SwingConstants.CENTER);
        noteLabel.setPreferredSize(new Dimension(DIMBINDLABEL, DIMBINDLABEL));
        noteLabel.setForeground(Color.WHITE);
        labelPanel.add(noteLabel);

        keyBindLabel = new JLabel(keyBind, SwingConstants.CENTER);
        keyBindLabel.setPreferredSize(new Dimension(DIMBINDLABEL, DIMBINDLABEL));
        keyBindLabel.setForeground(Color.WHITE);
        keyBindLabel.setBackground(Color.DARK_GRAY);
        keyBindLabel.setBorder(new LineBorder(Color.DARK_GRAY, 2, false));
        keyBindLabel.setOpaque(true);
        labelPanel.add(keyBindLabel);

        add(labelPanel);

        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        setBackground(Color.BLACK);
        setBorder(new LineBorder(Color.DARK_GRAY, 2, true));
        setFocusable(true);

    }

    @Override
    public void keyPressed() {
        setBackground(Color.RED);
        keyBindLabel.setBackground(Color.ORANGE);
    }

    @Override
    public void keyReleased() {
        setBackground(Color.BLACK);
        keyBindLabel.setBackground(Color.DARK_GRAY);
    }

    public String getNote() {
        return note;
    }
}
