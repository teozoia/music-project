package it.unimi.di.musica.view;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

public class Window extends JFrame implements PropertyChangeListener {

    private static final String TITLE = "Clef";
    private static final int WIDTH = 800;
    private static final int HEIGHT = 800;

    private JPanel basePanel;

    private JPanel statusPanel;
    private JPanel leftStatusPanel;
    private JPanel rightStatusPanel;
    private ImageIcon noteGreen;
    private ImageIcon noteRed;
    private JLabel name;
    private JLabel status;
    private JLabel levelLabel;
    private JLabel counterLabel;
    private JLabel hitLabel;
    private JLabel missLabel;

    private JPanel scorePanel;
    private JLabel scoreLabel;

    private JPanel keyboardPanel;
    private JLayeredPane pianoPanel;

    private JPanel pianoWhitePanel;
    private JPanel pianoBlackPanel1;
    private JPanel pianoBlackPanel2;
    private List<Key> wKeys;
    private List<Key> bKeys1;
    private List<Key> bKeys2;

    private String scoreString;
    private Timer timer;

    public Window(){
        super(TITLE);
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        basePanel = new JPanel(new BorderLayout());
        scorePanel = new JPanel();
        keyboardPanel = new JPanel();
        pianoPanel  = new JLayeredPane();
        pianoPanel.setPreferredSize(new Dimension(80 * 7, 200));

        // Setting status panel
        statusPanel = new JPanel(new BorderLayout());
        leftStatusPanel = new JPanel(new BorderLayout());
        rightStatusPanel = new JPanel(new FlowLayout(FlowLayout.LEADING, 20 ,0));
        statusPanel.add(leftStatusPanel, BorderLayout.WEST);
        statusPanel.add(rightStatusPanel, BorderLayout.EAST);

        name = new JLabel("Clef", SwingConstants.CENTER);
        statusPanel.add(name, BorderLayout.CENTER);
        name.setFont(new Font("Arial", Font.BOLD, 65));

        statusPanel.setBackground(Color.WHITE);
        leftStatusPanel.setBackground(Color.WHITE);
        rightStatusPanel.setBackground(Color.WHITE);
        scorePanel.setBackground(Color.WHITE);
        keyboardPanel.setBackground(Color.WHITE);

        noteGreen = new ImageIcon(new ImageIcon(getClass().getClassLoader().getResource("noteGreen.png"))
                .getImage().getScaledInstance(40, 62, Image.SCALE_AREA_AVERAGING), "Hit");
        noteRed = new ImageIcon(new ImageIcon(getClass().getClassLoader().getResource("noteRed.png"))
                .getImage().getScaledInstance(40, 62, Image.SCALE_AREA_AVERAGING), "Miss");

        levelLabel = new JLabel("LEVEL 1");
        counterLabel = new JLabel("Time left: 10 sec.");
        hitLabel = new JLabel("0", noteGreen, JLabel.CENTER);
        missLabel = new JLabel("0", noteRed, JLabel.CENTER);

        levelLabel.setFont(new Font("Arial", Font.PLAIN, 32));
        counterLabel.setFont(new Font("Arial", Font.PLAIN, 14));

        hitLabel.setFont(new Font("Arial", Font.PLAIN, 32));
        missLabel.setFont(new Font("Arial", Font.PLAIN, 32));

        leftStatusPanel.add(levelLabel, BorderLayout.NORTH);
        leftStatusPanel.add(counterLabel);
        rightStatusPanel.add(hitLabel);
        rightStatusPanel.add(missLabel);

        status = new JLabel("", SwingConstants.CENTER);
        status.setForeground(Color.RED);
        leftStatusPanel.add(status, BorderLayout.SOUTH);
        status.setFont(new Font("Arial", Font.PLAIN, 14));

        // Setting scorePanel
        scoreString = "<html>2dbkghhk<span style='color:red'>c</span>ow<span style='color:red'>k</span>d-1eggfhdhsfv<span style='background-color:yellow'>n</span>a-3lxyjxyjxxyjyjfb-<br>1vxcvdssnabir5disb2vdasycdupv=<br>ir5disb2vdasy</html>";
        scoreLabel = new JLabel(scoreString);
        scoreLabel.setFont(new Font("StaffClefPitchesEasy", Font.PLAIN, 40));
        scorePanel.add(scoreLabel, BorderLayout.CENTER);

        // Setting keyboards
        pianoWhitePanel = new JPanel(new FlowLayout(FlowLayout.LEADING, 0,0));
        pianoBlackPanel1 = new JPanel(new FlowLayout(FlowLayout.LEADING, 40 ,0));
        pianoBlackPanel2 = new JPanel(new FlowLayout(FlowLayout.LEADING, 40 ,0));
        wKeys = new ArrayList<Key>();
        bKeys1 = new ArrayList<Key>();
        bKeys2 = new ArrayList<Key>();

        pianoBlackPanel1.setBackground(new Color(255,25,255,0));
        pianoBlackPanel2.setBackground(new Color(255,25,255,0));

        wKeys.add(new WhiteKey("A", "C"));
        wKeys.add(new WhiteKey("S", "D"));
        wKeys.add(new WhiteKey("D", "E"));

        wKeys.add(new WhiteKey("F", "F"));
        wKeys.add(new WhiteKey("G", "G"));
        wKeys.add(new WhiteKey("H", "A"));
        wKeys.add(new WhiteKey("J", "B"));

        bKeys1.add(new BlackKey("W", "C#"));
        bKeys1.add(new BlackKey("E", "D#"));

        bKeys2.add(new BlackKey("T", "F#"));
        bKeys2.add(new BlackKey("Y", "G#"));
        bKeys2.add(new BlackKey("U", "A#"));

        for(Component c : wKeys)
            pianoWhitePanel.add(c);

        for(Component c : bKeys1)
            pianoBlackPanel1.add(c);
        for(Component c : bKeys2)
            pianoBlackPanel2.add(c);

        pianoWhitePanel.setBounds(0, 0, 80 * 7, 200);
        pianoBlackPanel1.setBounds(20, 0, 80 * 7, 200);
        pianoBlackPanel2.setBounds(260, 0, 80 * 7, 200);
        pianoPanel.add(pianoWhitePanel, new Integer(0));
        pianoPanel.add(pianoBlackPanel1, new Integer(1));
        pianoPanel.add(pianoBlackPanel2, new Integer(1));

        pianoBlackPanel1.setOpaque(false);
        pianoBlackPanel2.setOpaque(false);
        keyboardPanel.add(pianoPanel, BorderLayout.CENTER);

        basePanel.add(statusPanel, BorderLayout.NORTH);
        basePanel.add(scorePanel, BorderLayout.CENTER);
        basePanel.add(keyboardPanel, BorderLayout.SOUTH);

        add(basePanel);

        pack();
        setVisible(true);
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        switch(evt.getPropertyName()){
            case "newLevel":
                levelLabel.setText("LEVEL " + evt.getNewValue().toString());
                break;
            case "updateScore":
                updateScore((String) evt.getNewValue());
                counterLabel.setText("Time left: 10 sec.");
                break;
            case "hit":
                hitLabel.setText(evt.getNewValue().toString());
                break;
            case "miss":
                missLabel.setText(evt.getNewValue().toString());
                break;
            case "complete":
                JOptionPane.showMessageDialog(this, evt.getNewValue(), "Level complete", 1);
                break;
            case "timeChange":
                counterLabel.setText("Time left: " + evt.getNewValue() + " sec.");
                break;
            case "timeUp":
                updateStatus("Too slow!");
                break;
        }
    }

    private void updateScore(String score) {
        scoreString = score;
        scoreLabel.setText(scoreString);
    }

    // keyListCode: 0=white, 1=black1, 2=black2
    public void pressedKey(int keyListCode, int index){
        switch(keyListCode){
            case 0:
                wKeys.get(index).keyPressed();
                break;
            case 1:
                bKeys1.get(index).keyPressed();
                break;
            case 2:
                bKeys2.get(index).keyPressed();
                break;
        }
    }

    public void releasedKey(int keyListCode, int index){
        switch(keyListCode){
            case 0:
                wKeys.get(index).keyReleased();
                break;
            case 1:
                bKeys1.get(index).keyReleased();
                break;
            case 2:
                bKeys2.get(index).keyReleased();
                break;
        }
    }

    public void addMouseListenerWhite(MouseListener l) {
        for(Key c : wKeys)
            c.addMouseListener(l);
    }

    public void addMouseListenerBlack(MouseListener l) {
        for(Component c : bKeys1)
            c.addMouseListener(l);
        for(Component c : bKeys2)
            c.addMouseListener(l);
    }

    private void updateStatus(String newStatus){
        status.setText(newStatus);
        timer = new Timer(3000,new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                clearStatus();
            }
        });
        timer.start();
    }

    private void clearStatus(){
        timer.stop();
        status.setText("");
    }

}
