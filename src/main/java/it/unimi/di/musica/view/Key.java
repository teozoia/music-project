package it.unimi.di.musica.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Arrays;

public abstract class Key extends JPanel {

    protected PropertyChangeSupport changes = new PropertyChangeSupport(this); // ack changes to controller

    public abstract void keyPressed();

    public abstract void keyReleased();

    public void addPropertyChangeListener(PropertyChangeListener l) {
        changes.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        changes.removePropertyChangeListener(l);
    }
}
