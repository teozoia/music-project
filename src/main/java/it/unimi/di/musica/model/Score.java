package it.unimi.di.musica.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/*
 * La classe Score è un semplice wrapper di una lista di Note. Il valore
 * delle note nella lista è assoluto e non dipende da nessuna chiave, ne dalle
 * alterazioni in chiave.
 */

public abstract class Score implements Iterable<Note> {

    protected List<Note> notes;

    public Score(){
        notes = new ArrayList<Note>();
    }

    public int countNotes(){
        return notes.size();
    }

    public void addNote(Note n){
        notes.add(n);
    }

    public void addNote(int index, Note n){
        notes.add(index, n);
    }

    public void addNotes(List<Note> notes){
        this.notes.addAll(notes);
    }

    public Note getNote(int index) throws NoteException {
        return notes.get(index);
    }

    public void clear(){
        notes.clear();
    }

    public void setNote(int index, Note n){
        notes.set(index, n);
    }

    public void removeNote(int index){
        notes.remove(index);
    }

    public int size(){
        return notes.size();
    }

    @Override
    public Iterator<Note> iterator() {
        return notes.iterator();
    }
}
