package it.unimi.di.musica.model;

import java.util.*;
import static java.util.Map.entry;

public class ClefScore extends Score implements Iterable<Note>{

    private Map<Integer, String> glyphTreble = Map.ofEntries(
            entry(83, "u"),     // b5
            entry(82, "Y"), // a5#
            entry(81, "y"),     // a5
            entry(80, "T"), // g5#
            entry(79, "t"),     // g5
            entry(78, "R"), // f5#
            entry(77, "r"),     // f5
            entry(76, "e"),     // e5
            entry(75, "W"), // d5#
            entry(74, "w"),     // d5
            entry(73, "Q"), // c5#
            entry(72, "q"),     // c5, midi 72

            entry(71, "j"),     // b4
            entry(70, "H"), // b4#
            entry(69, "h"),     // a4
            entry(68, "G"), // g4#
            entry(67, "g"),     // g4
            entry(66, "F"), // f4#
            entry(65, "f"),     // f4
            entry(64, "d"),     // e4
            entry(63, "S"), // d4#
            entry(62, "s"),     // d4
            entry(61, "A"), // c4#
            entry(60, "a"),     // c4, midi 60

            entry(59, "m"),     // b3
            entry(58, "N"), // a3#
            entry(57, "n"),     // a3
            entry(56, "B"), // g3#
            entry(55, "b"),     // g3
            entry(54, "V"), // f3#
            entry(53, "v"),     // f3
            entry(52, "c"),     // e3
            entry(51, "X"), // d3#
            entry(50, "x"),     // d3
            entry(49, "Z"), // c3#
            entry(48, "z")      // c3, midi 48
    );

    private Map<Integer, String> glyphBass = Map.ofEntries(
            entry(83, "5"),
            entry(82, "$"),
            entry(81, "4"),
            entry(80, "#"),
            entry(79, "3"),
            entry(78, "@"),
            entry(77, "2"),
            entry(76, "1"),
            entry(75, "U"),
            entry(74, "u"),
            entry(73, "Y"),
            entry(72, "y"),

            entry(71, "t"),
            entry(70, "R"),
            entry(69, "r"),
            entry(68, "E"),
            entry(67, "e"),
            entry(66, "W"),
            entry(65, "w"),
            entry(64, "q"),
            entry(63, "J"),
            entry(62, "j"),
            entry(61, "H"),
            entry(60, "h"),

            entry(59, "g"),
            entry(58, "F"),
            entry(57, "f"),
            entry(56, "D"),
            entry(55, "d"),
            entry(54, "S"),
            entry(53, "s"),
            entry(52, "a"),
            entry(51, "M"),
            entry(50, "m"),
            entry(49, "N"),
            entry(48, "n")
    );

    private Map<Integer, String> glyphAlto = Map.ofEntries(
            entry(83, "y"),
            entry(82, "T"),
            entry(81, "t"),
            entry(80, "R"),
            entry(79, "r"),
            entry(78, "E"),
            entry(77, "e"),
            entry(76, "w"),
            entry(75, "Q"),
            entry(74, "q"),
            entry(73, "J"),
            entry(72, "j"),

            entry(71, "h"),
            entry(70, "G"),
            entry(69, "g"),
            entry(68, "F"),
            entry(67, "f"),
            entry(66, "D"),
            entry(65, "d"),
            entry(64, "s"),
            entry(63, "A"),
            entry(62, "a"),
            entry(61, "M"),
            entry(60, "m"),

            entry(59, "n"),
            entry(58, "B"),
            entry(57, "b"),
            entry(56, "V"),
            entry(55, "v"),
            entry(54, "C"),
            entry(53, "c"),
            entry(52, "x"),
            entry(51, "Z"),
            entry(50, "z"),
            entry(49, "p"),
            entry(48, "p")
    );

    private Map<Integer, String> glyphTenor = Map.ofEntries(
            entry(83, "2"),
            entry(82, "!"),
            entry(81, "1"),
            entry(80, "Y"),
            entry(79, "y"),
            entry(78, "T"),
            entry(77, "t"),
            entry(76, "r"),
            entry(75, "E"),
            entry(74, "e"),
            entry(73, "W"),
            entry(72, "w"),

            entry(71, "q"),
            entry(70, "J"),
            entry(69, "j"),
            entry(68, "H"),
            entry(67, "h"),
            entry(66, "G"),
            entry(65, "g"),
            entry(64, "f"),
            entry(63, "D"),
            entry(62, "d"),
            entry(61, "S"),
            entry(60, "s"),

            entry(59, "a"),
            entry(58, "M"),
            entry(57, "m"),
            entry(56, "N"),
            entry(55, "n"),
            entry(54, "B"),
            entry(53, "b"),
            entry(52, "v"),
            entry(51, "C"),
            entry(50, "c"),
            entry(49, "X"),
            entry(48, "x")
    );

    private Clef clef;
    private List<Accidental> alt;

    public ClefScore(){
        Random rnd = new Random();
        Clef clefList[] = Clef.values();

        this.clef = clefList[rnd.nextInt(4)]; // fix solo le prime 4 chiavi
        alt = Arrays.asList(Accidental.NATURAL, // C
                Accidental.NATURAL, // D
                Accidental.NATURAL, // E
                Accidental.NATURAL, // F
                Accidental.NATURAL, // G
                Accidental.NATURAL, // A
                Accidental.NATURAL); // B
    }

    public ClefScore(Clef clef, List<Accidental> alt){
        super();
        this.clef = clef;
        this.alt = alt;
    }

    public Clef getClef() {
        return clef;
    }

    @Override
    public Note getNote(int index) {
        Note n = notes.get(index);
        Pitch p = n.getPitch();
        Accidental a = n.getAccidental();

        try {
            return new Note(p, n.getOctave(), Accidental.getAccidental( a.ordinal() - alt.get(p.ordinal()).ordinal() ));
        } catch (NoteException e) {
            System.out.println("Cannot convert to selected clef score");
        }
        return null;
    }

    @Override
    public Iterator<Note> iterator() {

        return new Iterator<Note>() {
            int i = 0;

            @Override
            public boolean hasNext() {
                return i < notes.size();
            }

            @Override
            public Note next() {
                Note n = getNote(i);
                i++;
                return n;
            }
        };
    }

    public String getClefGlyph() {
        switch(clef){
            case TREBLE: return "&";
            case ALTO: return "}";
            case TENOR: return "{";
            case BASS: return "?";
            // Miglioramento aggiungere le chiavi mancanti nel font
            default: return "";
        }
    }


    public String getNotesGlyph() {
        Map<Integer, String> glyph = null;
        String notesGlyph = "";

        switch(clef){
            case TREBLE: glyph = glyphTreble; break;
            case ALTO:  glyph = glyphAlto; break;
            case TENOR:  glyph = glyphTenor; break;
            case BASS: glyph = glyphBass; break;
        }

        for(int i = 0; i < notes.size(); i++){
            Note cleffedNote = getNote(i);
            notesGlyph += glyph.get(cleffedNote.toMidi());
        }

        return notesGlyph;
    }
}
