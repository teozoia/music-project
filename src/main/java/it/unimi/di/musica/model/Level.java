package it.unimi.di.musica.model;

import jm.util.Play;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Level {

    private static final int MAX_LEVEL = 10;
    private static final int MIN_LEVEL = 1;
    private static final float HITRATE_PASS = .7f;
    private static final int NOTE_COUNTER_BASE = 15;
    private static final int BASE_TIME = 10; // 10 sec.

    private int level;
    private List<ClefScore> scoreList;
    private int targetScoreIndex;
    private int targetNoteIndex;
    private int hit;
    private int miss;

    private Timer timer;
    private int elapsedTime;

    private PropertyChangeSupport changes = new PropertyChangeSupport(this);

    public Level(){
        scoreList = new ArrayList<ClefScore>();
        scoreList.clear();
    }

    public void init(){
        level = 1;
        generateLevel();
    }

    private void startTimer() {
        if(timer != null)
            timer.stop();

        if(level < 5)
            elapsedTime = BASE_TIME;
        else
            elapsedTime = BASE_TIME / 2;

        timer = new Timer(1000,new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                int oldTime = elapsedTime;
                elapsedTime--;
                changes.firePropertyChange("timeChange", oldTime, elapsedTime);

                if(elapsedTime <= 0){
                    timer.stop();
                    changes.firePropertyChange("timeUp", null, null);
                    playWrongNote();
                }
            }
        });
        timer.start();
    }

    public void levelUp(){
        int oldLevel = level;
        float rate = (float)hit / (hit + miss);
        String rateText = "Hitrate: " + rate + " (H: " + hit + ", M: " + miss + ")";

        if(level >= MAX_LEVEL) {
            changes.firePropertyChange("win", null, null);
        }else{
            level++;
            changes.firePropertyChange("newLevel", oldLevel, level);
            generateLevel();
        }

        changes.firePropertyChange("complete", null, rateText);
    }

    public void levelDown(){
        int oldLevel = level;
        float rate = (float)hit / (hit + miss);
        String rateText = "Hitrate: " + rate + " (H: " + hit + ", M: " + miss + ")";

        if(level >= MIN_LEVEL) {
            if(level > 1)
                level--;
            changes.firePropertyChange("newLevel", oldLevel, level);
            generateLevel();
        }

        changes.firePropertyChange("complete", null, rateText);
    }

    private void generateLevel(){
        scoreList.clear();
        int clefCounter = getClefCounter();

        for(int i = 0; i < clefCounter; i++){
            ClefScore c = new ClefScore(); // random clef
            Note oldNote = null;
            for(int j = 0; j < NOTE_COUNTER_BASE; j++) {
                Note nextNote = generateNextNote(level, oldNote, c.getClef());
                c.addNote(nextNote);
                oldNote = nextNote;
            }
            scoreList.add(c);
        }

        int oldHit = hit;
        int oldMiss = miss;

        targetScoreIndex = 0;
        targetNoteIndex = 0;
        hit = 0;
        miss = 0;

        changes.firePropertyChange("hit", oldHit, this.hit);
        changes.firePropertyChange("miss", oldMiss, this.miss);
        changes.firePropertyChange("updateScore", null, toString());
    }

    private Note generateNextNote(int level, Note oldNote, Clef clef) {
        Random rnd = new Random();
        int midiLower = 65;
        int midiHigher = 76;
        int midiLowerRange;
        int midiHigherRange;

        if(level <= 5){
            switch(clef){
                case TREBLE:
                    midiLower = 65;
                    midiHigher = 76;
                    break;
                case BASS:
                    midiLower = 57;
                    midiHigher = 67;
                    break;
                case ALTO:
                    midiLower = 67;
                    midiHigher = 77;
                    break;
                case TENOR:
                    midiLower = 64;
                    midiHigher = 74;
                    break;
            }
        }else{
            switch(clef){
                case TREBLE:
                    midiLower = 52;
                    midiHigher = 81;
                    break;
                case BASS:
                    midiLower = 48;
                    midiHigher = 73;
                    break;
                case ALTO:
                    midiLower = 53;
                    midiHigher = 83;
                    break;
                case TENOR:
                    midiLower = 50;
                    midiHigher = 79;
                    break;
            }
        }

        if(oldNote == null)
            return Note.fromMidi(rnd.nextInt(midiHigher-midiLower)+midiLower);

        if(level <= 5){
            midiLowerRange = oldNote.toMidi() - 5;
            midiHigherRange = oldNote.toMidi() + 5;
        }else{
            midiLowerRange = oldNote.toMidi() - 10;
            midiHigherRange = oldNote.toMidi() + 10;
        }

        if(midiLowerRange < midiLower)
            midiLowerRange = midiLower;
        if(midiHigherRange > midiHigher)
            midiHigherRange = midiHigher;

        return Note.fromMidi(rnd.nextInt(midiHigherRange-midiLowerRange)+midiLowerRange);
    }

    private int getClefCounter() {
        if(level <= 3) {
            return 2;
        }else if(level <= 5) {
            return 3;
        }else if(level <= 8) {
            return 4;
        }else return 5;
    }

    public Note getTargetNote(){
        return scoreList.get(targetScoreIndex).getNote(targetNoteIndex);
    }

    public boolean playNote(Note n){
        ClefScore score = scoreList.get(targetScoreIndex);
        Note targetNote = score.getNote(targetNoteIndex);

        Runnable play = () -> {
            Play player = new Play();
            player.midi(new jm.music.data.Note(n.toMidi(), 0.5, 127), false);
        };
        new Thread(play).start();

        boolean hit = (targetNote.compareTo(n) % 12) == 0;
        if(hit) {
            int oldHit = this.hit;
            this.hit++;
            System.out.println("hit " + targetNote.compareTo(n) + " n=" + n + " target=" + targetNote);
            changes.firePropertyChange("hit", oldHit, this.hit);
        }else {
            int oldMiss = this.miss;
            this.miss++;
            System.out.println("miss " + targetNote.compareTo(n) + " n=" + n + " target=" + targetNote);
            changes.firePropertyChange("miss", oldMiss, this.miss);
        }

        if(targetNoteIndex < scoreList.get(targetScoreIndex).size() - 1) {
            targetNoteIndex++;
            startTimer();
        }else{
            if(targetScoreIndex < scoreList.size() - 1) {
                targetScoreIndex++;
                targetNoteIndex = 0;
                startTimer();
            }else{
                timer.stop();
                triggerNewLevel();
            }
        }
        changes.firePropertyChange("updateScore", null, toString());

        return hit;
    }

    public void playWrongNote(){

        int oldMiss = this.miss;
        this.miss++;
        changes.firePropertyChange("miss", oldMiss, this.miss);

        if(targetNoteIndex < scoreList.get(targetScoreIndex).size() - 1) {
            targetNoteIndex++;
            startTimer();
        }else{
            if(targetScoreIndex < scoreList.size() - 1) {
                targetScoreIndex++;
                targetNoteIndex = 0;
                startTimer();
            }else{
                timer.stop();
                triggerNewLevel();
            }
        }
        changes.firePropertyChange("updateScore", null, toString());
    }

    private void triggerNewLevel(){
        float rate = (float)hit / (hit+miss);
        System.out.println("New level hit=" + hit + " miss=" + miss + " rate=" + rate);

        if(rate >= HITRATE_PASS)
            levelUp();
        else
            levelDown();
    }

    public String toString(){
        String out = "<html><p style=\"width:700px;\">";
        String rawScore = "";

        for (int i = 0; i < scoreList.size(); i++){
            rawScore += "=";
            rawScore += scoreList.get(i).getClefGlyph();
            String notesGlyph = scoreList.get(i).getNotesGlyph();
            if(targetScoreIndex == i){
                String n = "<span style=\"background-color:yellow;\">" +
                        notesGlyph.charAt(targetNoteIndex) + "</span>";
                String start = notesGlyph.substring(0, targetNoteIndex);
                String end = notesGlyph.substring(targetNoteIndex + 1);
                rawScore += start + n + end;
            }else{
                rawScore += notesGlyph;
            }
            rawScore += "|";
        }
        out += rawScore;
        return out + "</p></html>";
    }

    public void addPropertyChangeListener(PropertyChangeListener l) {
        changes.addPropertyChangeListener(l);
    }

    public void removePropertyChangeListener(PropertyChangeListener l) {
        changes.removePropertyChangeListener(l);
    }
}
