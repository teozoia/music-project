package it.unimi.di.musica.model;

public enum Accidental {
  DOUBLEFLAT,
  FLAT,
  NATURAL,
  SHARP,
  DOUBLESHARP;

  public int getSemitones() {
    switch(this){
      case DOUBLEFLAT: return -2;
      case FLAT: return -1;
      case NATURAL: return 0;
      case SHARP: return 1;
      case DOUBLESHARP: return 2;
    }
    return Integer.MAX_VALUE;
  }

  public static Accidental getAccidental(int value) {
    switch(value){
      case -2: return DOUBLEFLAT;
      case -1: return FLAT;
      case 0: return NATURAL;
      case 1: return SHARP;
      case 2: return DOUBLESHARP;
    }
    return null;
  }

  @Override
  public String toString() {
    switch(this){
      case DOUBLEFLAT: return "bb";
      case FLAT: return "b";
      case SHARP: return "#";
      case DOUBLESHARP: return "x";
    }
    return "";
  }
}
