package it.unimi.di.musica.model;

public class Note implements Comparable{
    private static final Pitch BASE_PITCH = Pitch.C;
    private static final Integer BASE_OCTAVE = 4;
    private static final Accidental BASE_ACCIDENTAL = Accidental.NATURAL;

    private Pitch pitch;
    private Integer octave;
    private Accidental accidental;

    public Note(Pitch pitch, int octave, Accidental accidental) throws NoteException {
        if(octave < 0 || octave > 9)
            throw new NoteException("Octave must be from C0 to B9");
        if(octave == 0 && pitch == Pitch.C && accidental.getSemitones() < Accidental.NATURAL.getSemitones())
            throw new NoteException("In octave:0 the pitch: C can only have accidental neutral or positive");

        this.pitch = pitch;
        this.octave = octave;
        this.accidental = accidental;
    }

    public Pitch getPitch() {
        return pitch;
    }

    public int getOctave() {
        return octave;
    }

    public Accidental getAccidental() {
        return accidental;
    }

    @Override
    public String toString() {
        return pitch.name() + octave.toString() + accidental.toString();
    }

    @Override
    public int compareTo(Object o) {
        Note n = (Note)o;

        return (this.octave - n.getOctave()) * 12 +
                (this.pitch.getSemitones() - n.getPitch().getSemitones()) +
                (this.accidental.getSemitones() - n.getAccidental().getSemitones());
    }

    public int semitonesFromBase(){
        try {
            Note c0 = new Note(BASE_PITCH, 0, BASE_ACCIDENTAL);
            return this.compareTo(c0);
        } catch (NoteException e) {
            e.printStackTrace();
        }
        return -1;
    }

    // From 0-131, real MIDI 0/12-127
    public int toMidi(){
        try {
            Note c4 = new Note(BASE_PITCH, BASE_OCTAVE, BASE_ACCIDENTAL);
            return this.compareTo(c4) + 60;
        } catch (NoteException e) {
            e.printStackTrace();
        }
        return Integer.MAX_VALUE;
    }

    public static Note fromMidi(int midi) {

        Pitch pitch;
        int octave;
        Accidental accidental;

        octave = (midi / 12) - 1;
        pitch = Pitch.getPitch(midi - (midi / 12) * 12);
        accidental = Accidental.getAccidental(midi - ((midi / 12) * 12) - pitch.getSemitones());

        try {
            return new Note(pitch, octave, accidental);
        } catch (NoteException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int semitonesInterval(Note other){
        return (other.semitonesFromBase() - this.semitonesFromBase()) % 12;
    }

}
