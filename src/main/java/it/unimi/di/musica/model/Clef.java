package it.unimi.di.musica.model;

public enum Clef {
  TREBLE,
  ALTO,
  TENOR,
  BASS,
  SOPRANO,
  MEZZOSOPRANO,
  BARITONE;

  @Override
  public String toString() {
    switch(this){
      case TREBLE: return "Treble (G4)";
      case SOPRANO: return "Soprano (C4)";
      case MEZZOSOPRANO: return "Mezzosoprano (C4)";
      case ALTO: return "Alto (C4)";
      case TENOR: return "Tenor (C4)";
      case BARITONE: return "Baritone (F3)";
      case BASS: return "Bass (F3)";
    }
    return "";
  }
}
