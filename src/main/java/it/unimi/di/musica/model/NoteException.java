package it.unimi.di.musica.model;

public class NoteException extends Exception{
  public NoteException(String message){ super(message);}
}
