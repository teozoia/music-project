package it.unimi.di.musica.model;

public enum Pitch {
  C,
  D,
  E,
  F,
  G,
  A,
  B;

  public int getSemitones() {
    switch(this){
      case C: return 0;
      case D: return 2;
      case E: return 4;
      case F: return 5;
      case G: return 7;
      case A: return 9;
      case B: return 11;
    }
    return -1;
  }

  public static Pitch getPitch(int value) {
    switch(value){
      case 0:
      case 1:
        return C;
      case 2:
      case 3:
        return D;
      case 4:
        return E;
      case 5:
      case 6:
        return F;
      case 7:
      case 8:
        return G;
      case 9:
      case 10:
        return A;
      case 11:
        return B;
    }
    return null;
  }
}
