package it.unimi.di.musica.model;

import net.jqwik.api.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

public class PBNoteTest {

    private final int EXAMPLES = 1000;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Report({Reporting.GENERATED, Reporting.FALSIFIED})
    @Property(tries = EXAMPLES, seed = "12345")
    public void testPBValidNote(@ForAll("validNote") Integer[] noteValues){

        Pitch pitch = Pitch.values()[noteValues[0]];
        int octave = noteValues[1];
        Accidental accidental = Accidental.values()[noteValues[2]];

        try {
            Note note = new Note(pitch, octave, accidental);
        } catch (NoteException e) {
            fail(e.getMessage());
        }
    }

    @Report({Reporting.GENERATED, Reporting.FALSIFIED})
    @Property(tries = EXAMPLES, seed = "12345")
    public void testPBSemitonesFromBase(@ForAll("validNote") Integer[] noteValues){

        Pitch pitch = Pitch.values()[noteValues[0]];
        int octave = noteValues[1];
        Accidental accidental = Accidental.values()[noteValues[2]];

        try {
            Note note = new Note(pitch, octave, accidental);
            assertEquals(octave * 12 + pitch.getSemitones() + accidental.getSemitones(), note.semitonesFromBase());
        } catch (NoteException e) {
            fail(e.getMessage());
        }
    }

    @Report({Reporting.GENERATED, Reporting.FALSIFIED})
    @Property(tries = EXAMPLES, seed = "12345")
    public void testPBCompare(@ForAll("validNote") Integer[] nValues1, @ForAll("validNote") Integer[] nValues2){

        Pitch pitch1 = Pitch.values()[nValues1[0]];
        int octave1 = nValues1[1];
        Accidental accidental1 = Accidental.values()[nValues1[2]];

        Pitch pitch2 = Pitch.values()[nValues2[0]];
        int octave2 = nValues2[1];
        Accidental accidental2 = Accidental.values()[nValues2[2]];

        try {
            Note n1 = new Note(pitch1, octave1, accidental1);
            Note n2 = new Note(pitch2, octave2, accidental2);

            int semitones1 = octave1 * 12 + pitch1.getSemitones() + accidental1.getSemitones();
            int semitones2 = octave2 * 12 + pitch2.getSemitones() + accidental2.getSemitones();

            assertEquals(semitones1 - semitones2, n1.compareTo(n2));
        } catch (NoteException e) {
            fail(e.getMessage());
        }
    }

    @Provide
    private Arbitrary<Integer[]> validNote() {

        Arbitrary<Integer> pitch = Arbitraries.integers().between(Pitch.C.ordinal(), Pitch.B.ordinal()); // 0 - 11
        Arbitrary<Integer> octave = Arbitraries.integers().between(0, 9);
        Arbitrary<Integer[]> note = pitch.flatMap( p -> octave.flatMap( o -> {
            int lowerBoundCorrection = 0;

            if(o == 0 && p == 0)
                lowerBoundCorrection = 2; // Avoid C0b or C0bb

            return Arbitraries
                    .integers()
                    .between(Accidental.DOUBLEFLAT.ordinal() + lowerBoundCorrection, Accidental.DOUBLESHARP.ordinal())
                    .map(a -> new Integer[] {p, o, a});
        }));

        return note;
    }

}
