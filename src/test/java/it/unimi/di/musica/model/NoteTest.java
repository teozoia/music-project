package it.unimi.di.musica.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NoteTest {

	@BeforeEach
	void setUp() {
	}

	@AfterEach
	void tearDown() {
	}

	@Test
	public void testC4Sharp(){

		Note n = null;
		try {
			n = new Note(Pitch.C, 4, Accidental.SHARP);
		} catch (NoteException e) {
			fail(e.getMessage());
		}
		assertEquals("C4#", n.toString());
	}

	@Test
	public void testB3bb(){
		Note n = null;
		try {
			n = new Note(Pitch.B, 3, Accidental.DOUBLEFLAT);
		} catch (NoteException e) {
			fail(e.getMessage());
		}
		assertEquals("B3bb", n.toString());
	}

	@Test
	public void testNoteOutOfBounds1(){
		Assertions.assertThrows(NoteException.class, () ->
			new Note(Pitch.D, -3, Accidental.NATURAL)
		);
	}

	@Test
	public void testNoteOutOfBounds2(){
		Assertions.assertThrows(NoteException.class, () ->
						new Note(Pitch.G, 17, Accidental.NATURAL)
		);
	}

	@Test
	public void testCompare1(){
		Note n1 = null;
		Note n2 = null;
		try {
			n1 = new Note(Pitch.B, 3, Accidental.DOUBLEFLAT);
			n2 = new Note(Pitch.B, 3, Accidental.DOUBLEFLAT);
		} catch (NoteException e) {
			fail(e.getMessage());
		}
		assertEquals(0, n1.compareTo(n2));
	}

	@Test
	public void testCompare2(){
		Note n1 = null;
		Note n2 = null;
		try {
			n1 = new Note(Pitch.C, 4, Accidental.NATURAL);
			n2 = new Note(Pitch.C, 4, Accidental.SHARP);
		} catch (NoteException e) {
			fail(e.getMessage());
		}
		assertEquals(-1, n1.compareTo(n2));
	}

	@Test
	public void testCompare3(){
		Note n1 = null;
		Note n2 = null;
		try {
			n1 = new Note(Pitch.C, 5, Accidental.NATURAL);
			n2 = new Note(Pitch.C, 4, Accidental.NATURAL);
		} catch (NoteException e) {
			fail(e.getMessage());
		}
		assertEquals(12, n1.compareTo(n2));
	}

	@Test
	public void testCompare4(){
		Note n1 = null;
		Note n2 = null;
		try {
			n1 = new Note(Pitch.C, 0, Accidental.NATURAL);
			n2 = new Note(Pitch.C, 4, Accidental.NATURAL);
		} catch (NoteException e) {
			fail(e.getMessage());
		}
		assertEquals(-48, n1.compareTo(n2));
	}

	@Test
	public void testSemitonesFromBase1(){
		Note n = null;
		try {
			n = new Note(Pitch.C, 0, Accidental.NATURAL);
		} catch (NoteException e) {
			fail(e.getMessage());
		}
		assertEquals(0, n.semitonesFromBase());
	}

	@Test
	public void testSemitonesFromBase2(){
		Note n = null;
		try {
			n = new Note(Pitch.C, 4, Accidental.NATURAL);
		} catch (NoteException e) {
			fail(e.getMessage());
		}
		assertEquals(48, n.semitonesFromBase());
	}

	@Test
	public void testSemitonesFromBase3(){
		Note n = null;
		try {
			n = new Note(Pitch.A, 4, Accidental.NATURAL);
		} catch (NoteException e) {
			fail(e.getMessage());
		}
		assertEquals(48 + 9, n.semitonesFromBase());
	}

	@Test
	public void testSemitonesFromBase4(){
		Note n = null;
		try {
			n = new Note(Pitch.B, 9, Accidental.NATURAL);
		} catch (NoteException e) {
			fail(e.getMessage());
		}
		assertEquals(48 + 71, n.semitonesFromBase());
	}

	@Test
	public void testToMidi1(){
		Note n = null;
		try {
			n = new Note(Pitch.C, 4, Accidental.NATURAL);
		} catch (NoteException e) {
			fail(e.getMessage());
		}
		assertEquals(60, n.toMidi());
	}

	@Test
	public void testToMidi2(){
		Note n = null;
		try {
			n = new Note(Pitch.B, 9, Accidental.NATURAL);
		} catch (NoteException e) {
			fail(e.getMessage());
		}
		assertEquals(131, n.toMidi());
	}

	@Test
	public void testToMidi3(){
		Note n = null;
		try {
			n = new Note(Pitch.E, 0, Accidental.FLAT);
		} catch (NoteException e) {
			fail(e.getMessage());
		}
		assertEquals(15, n.toMidi());
	}

	@Test
	public void testToMidi4(){
		Note n = null;
		try {
			n = new Note(Pitch.C, 0, Accidental.NATURAL);
		} catch (NoteException e) {
			fail(e.getMessage());
		}
		assertEquals(12, n.toMidi());
	}

	@Test
	public void testSemitonesInterval1(){
		Note n1 = null;
		Note n2 = null;
		try {
			n1 = new Note(Pitch.C, 0, Accidental.NATURAL);
			n2 = new Note(Pitch.D, 0, Accidental.NATURAL);
		} catch (NoteException e) {
			fail(e.getMessage());
		}
		assertEquals(2, n1.semitonesInterval(n2));
	}

	@Test
	public void testSemitonesInterval2(){
		Note n1 = null;
		Note n2 = null;
		try {
			n1 = new Note(Pitch.C, 2, Accidental.NATURAL);
			n2 = new Note(Pitch.D, 5, Accidental.NATURAL);
		} catch (NoteException e) {
			fail(e.getMessage());
		}
		assertEquals(2, n1.semitonesInterval(n2));
	}

	@Test
	public void testSemitonesInterval3(){
		Note n1 = null;
		Note n2 = null;
		try {
			n1 = new Note(Pitch.C, 4, Accidental.NATURAL);
			n2 = new Note(Pitch.F, 5, Accidental.SHARP);
		} catch (NoteException e) {
			fail(e.getMessage());
		}
		assertEquals(6, n1.semitonesInterval(n2));
	}

}
