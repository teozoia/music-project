package it.unimi.di.musica.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ClefScoreTest {

	List<Accidental> cMajAccidentals = new ArrayList<Accidental>();

	List<Accidental> gMajAccidentals; // Sol
	List<Accidental> dMajAccidentals; // Re
	List<Accidental> aMajAccidentals; // La
	List<Accidental> eMajAccidentals; // Mi
	List<Accidental> bMajAccidentals; // Si
	List<Accidental> fSharpMajAccidentals; // Fa#

	List<Accidental> fMajAccidentals; // Fa
	List<Accidental> bFlatMajAccidentals; // Sib
	List<Accidental> eFlatMajAccidentals; // Mib
	List<Accidental> aFlatMajAccidentals; // Lab
	List<Accidental> dFlatMajAccidentals; // Reb

	@BeforeEach
	void setUp() {
		cMajAccidentals.add(Accidental.NATURAL); // Do
		cMajAccidentals.add(Accidental.NATURAL); // Re
		cMajAccidentals.add(Accidental.NATURAL); // Mi
		cMajAccidentals.add(Accidental.NATURAL); // Fa
		cMajAccidentals.add(Accidental.NATURAL); // Sol
		cMajAccidentals.add(Accidental.NATURAL); // La
		cMajAccidentals.add(Accidental.NATURAL); // Si

		// Major sharp ones
		gMajAccidentals = new ArrayList<Accidental>(cMajAccidentals);
		gMajAccidentals.set(3, Accidental.SHARP); // set Fa#

		dMajAccidentals = new ArrayList<Accidental>(gMajAccidentals);
		dMajAccidentals.set(0, Accidental.SHARP); // set Do#

		aMajAccidentals = new ArrayList<Accidental>(dMajAccidentals);
		aMajAccidentals.set(4, Accidental.SHARP); // set Sol#

		eMajAccidentals = new ArrayList<Accidental>(aMajAccidentals);
		eMajAccidentals.set(1, Accidental.SHARP); // set Re#

		bMajAccidentals = new ArrayList<Accidental>(eMajAccidentals);
		bMajAccidentals.set(5, Accidental.SHARP); // set La#

		fSharpMajAccidentals = new ArrayList<Accidental>(bMajAccidentals);
		fSharpMajAccidentals.set(3, Accidental.SHARP); // set Fa#

		// Major flat ones
		fMajAccidentals = new ArrayList<Accidental>(cMajAccidentals);
		fMajAccidentals.set(6, Accidental.FLAT); // set Sib

		bFlatMajAccidentals = new ArrayList<Accidental>(fMajAccidentals);
		bFlatMajAccidentals.set(2, Accidental.FLAT); // set Mib

		eFlatMajAccidentals = new ArrayList<Accidental>(bFlatMajAccidentals);
		eFlatMajAccidentals.set(5, Accidental.FLAT); // set Lab

		aFlatMajAccidentals = new ArrayList<Accidental>(eFlatMajAccidentals);
		aFlatMajAccidentals.set(1, Accidental.FLAT); // set Reb

		dFlatMajAccidentals = new ArrayList<Accidental>(aFlatMajAccidentals);
		dFlatMajAccidentals.set(4, Accidental.FLAT); // set Solb
	}

	@AfterEach
	void tearDown() {
	}

	@Test
	public void testGmajScoreConversion1(){

		Note f4Sharp = null;
		try {
			f4Sharp = new Note(Pitch.F, 4, Accidental.SHARP);
		} catch (NoteException e) {
			fail(e.getMessage());
		}

		ClefScore score = new ClefScore(Clef.TREBLE, gMajAccidentals);
		score.addNote(f4Sharp);

		assertEquals("F4#", f4Sharp.toString());
		assertEquals(1, score.countNotes());
		assertEquals("F4", score.getNote(0).toString());
		// the MIDI test is unnecessary cause is already checked with the last line
	}

	@Test
	public void testGmajScoreConversion2(){

		Note f4 = null;
		try {
			f4 = new Note(Pitch.F, 4, Accidental.NATURAL);
		} catch (NoteException e) {
			fail(e.getMessage());
		}

		ClefScore score = new ClefScore(Clef.TREBLE, gMajAccidentals);
		score.addNote(f4);

		assertEquals("F4", f4.toString());
		assertEquals(1, score.countNotes());
		assertEquals("F4b", score.getNote(0).toString());
	}

	@Test
	public void testAFlatMajScoreConversion(){

		Note c4 = null;
		Note d4 = null;
		Note e4 = null;
		Note f4 = null;
		Note g4 = null;

		try {
			c4 = new Note(Pitch.C, 4, Accidental.NATURAL);
			d4 = new Note(Pitch.D, 4, Accidental.NATURAL);
			e4 = new Note(Pitch.E, 4, Accidental.NATURAL);
			f4 = new Note(Pitch.F, 4, Accidental.NATURAL);
			g4 = new Note(Pitch.G, 4, Accidental.NATURAL);
		} catch (NoteException e) {
			fail(e.getMessage());
		}

		ClefScore score = new ClefScore(Clef.TREBLE, aFlatMajAccidentals);
		score.addNote(c4);
		score.addNote(d4);
		score.addNote(e4);
		score.addNote(f4);
		score.addNote(g4);

		assertEquals("C4", c4.toString());
		assertEquals("D4", d4.toString());
		assertEquals("E4", e4.toString());
		assertEquals("F4", f4.toString());
		assertEquals("G4", g4.toString());

		assertEquals("C4", score.getNote(0).toString());
		// Reb nella scala di Lab, per suonare un D4 assoluto devo alzarlo di un semitono
		assertEquals("D4#", score.getNote(1).toString());
		assertEquals("E4#", score.getNote(2).toString());
		assertEquals("F4", score.getNote(3).toString());
		assertEquals("G4", score.getNote(4).toString());
	}


}
