# Clef
[![pipeline status](https://gitlab.com/teozoia/music-project/badges/master/pipeline.svg)](https://gitlab.com/teozoia/music-project/-/commits/master)
[![coverage report](https://gitlab.com/teozoia/music-project/badges/master/coverage.svg)](https://gitlab.com/teozoia/music-project/-/commits/master)

_Progetto di Programmazione per la musica (A. Baraté), traccia 9, Unimi._ Autore: Matteo Zoia, 941976

Implementazione di un gioco di tipo _serious game_ per l'apprendimento del setticlavio tramite Java Swing e JMusic.

---

**Requisiti per l'esecuzione del programma: installazione del font StaffClefPitchesEasy reperibile dal seguente [link](static/STAFCPE_.TTF), gradle e jdk11 o maggiore.**

## Traccia

Apprendimento setticlavio: prendendo spunto dal documento presente al link http://www.ludovico.net/download/papers/CSEDU2013.pdf, creare un’applicazione Swing che implementi un serious game per l’apprendimento della notazione sul setticlavio. Sarà presente un’interfaccia divisa in due parti, in cui a sinistra verranno visualizzate note scritte con chiavi diverse, e a destra una piccola tastiera per la riproduzione delle note scritte. Lo scopo è suonare il più velocemente possibile (e commettendo meno errori possibili) la sequenza di note proposte.

## Introduzione
Con la dicitura _serious game_ si intende una categoria di giochi e video giochi nei quali l'intrattenimento non è il solo scopo da raggiungere, ma é anche presente una forte componente didattica ed educativa. In questo progetto l'obiettivo è stato quello di implementare, tramite l'interfaccia grafica Swing in Java, un serious game sulla base dell'articolo _Serious games for music education: a mobile application to learn clef placement on the stave_ di A. Baratè e L. A. Ludovico. In particolare, si é voluta offrire una versione alternativa di iClef per PC, eseguibile tramite JVM e quindi disponibile sui diversi sistemi operativi.

Volendo riprodurre l'applicazione originale iClef su un supporto differente rispetto a quello originario, sono state apportate delle piccole modifiche alla traccia iniziale. Un esempio di queste modifiche é stata la ridisposizione e l'orientamento della componente _tastiera_ all'interno della schermata dell'applicazione. In iClef infatti é presente una tastiera di piccole dimensioni orientata in senso verticale, in Clef é presente una tastiera posta nella parte inferirore dell'applicazione e orientata in senso orizzontale. É stata fatta questa scelta grazie al maggior spazio disponibile sul supporto. 

## Design
Data la complessità dell'applicazione da implementare, si è deciso di procedere basando il progetto sul pattern MVC model-view-controller, cercando così di ridurre la complessità. Attraverso questo pattern si é quindi separata la gestione dei dati interni all'applicazione dalla parte di interfaccia (output), o dalla parte di controllo delle azioni svolte dall'utente.

### Model
Il concetto base all'interno del _model_ é quello di `Note`, classe che rappresenta una nota, la quale si basa su `Pitch`,  `Octave` e `Accidental`. La classe, oltre alle funzione getter/setter e altre funzioni proprie di una nota, offre anche una serie di utility che serviranno per l'interazione con altri componenti dell'applicazione. Un esempio di utility é:

```java
public int toMidi(); // Data una nota fornisce il codice MIDI
public static Note fromMidi(int midi); 	// Restituisce un'istanza di nota a partire da MIDI
```

**N.B.** il concetto di nota mappato nella classe `Note` rappresenta una nota assoluta, non posta in una partitura e quindi non dipendente da una specifica chiave. Ad esempio, una nota _La5#_ potrebbe essere rappresentata diversamente in base alla chiave, quindi potrebbe avere una diversa posizione sul rigo.

Per raccogliere piú note in una partitura é stata creata una classe di nome `ClefScore`, che contiene come campi della classe una chiave di tipo `Clef`, una lista di `Accidental` (che mappano le alterazioni in chiave) e una lista di `Note` (ereditata dalla classe astratta padre `Score`).

_La classe `Score`, qui non descritta, é una classe astratta che fa da wrapper per una lista di `Note`._

Si riporta, come parte saliente della classe `ClefScore`, il metodo utilizzato per ottenere il pentagramma musicale sottoforma di stringa stampabile. É stato implementato il metodo `public String getNotesGlyph()`, che, tramite il font musicale _StaffClefPitchesEasy_ e una mappa adeguata per ogni possibile chiave, traduce il valore delle note nel glifo corrispondente.

```java
private Map<Integer, String> glyphTreble = Map.ofEntries( // campo della classe
  entry(83, "u"), // b5
  entry(82, "Y"), // a5#
  entry(81, "y"), // a5
  ... );

public String getNotesGlyph() {
  ...
    switch(clef){
      case TREBLE: glyph = glyphTreble; break;
      case ALTO:  glyph = glyphAlto; break;
        ... 

  // tramite il metodo getNote(i) garantisco l'aggiunta delle alterazioni in chiave
  for(int i = 0; i < notes.size(); i++){ 
    notesGlyph += glyph.get(getNote(i).toMidi()); 
  }
  return notesGlyph;
}
```

La classe `Level` é la classe principale che contiene tutta la struttura del serious game; essa infatti possiede: una lista di `ClefScore`, il livello attuale `int level`, la quantitá di note giuste/sbagliate che sono state premute dall'utente `int hit` e `int miss`, l'indice della prossima nota da premere e una serie di altre variabili, utili al funzionamento dell'applicativo.

Possiede inoltre `private PropertyChangeSupport changes`, una variabile utile sopratutto in alcuni pattern, per la comunicazione tra i vari componenti del software. In particolarticolare é stata usata per comunicare i cambiamenti dal _model_ verso il componente _view_, che a fronte del cambiamento procederá all'update dell'interfaccia.

Il gioco si compone di 10 livelli di difficoltá nei quali gradualmente vengono generati una serie di brani musicali con 4 chiavi possibili, differenti per ogni brano. La generazione dei brani é svolta dal metodo `private void generateLevel()` che genera una alla volta tutte le _x_ note fissate che compongono un brano. Ogni nota é generata a partire dalla precedente, dal livello attuale e dalla chiave che si sta considerando ( metodo `private Note generateNextNote(int level, Note oldNote, Clef clef)`). Le note sono generate tramite un range che scala con la difficoltá.

Altri metodi degni di nota sono: `public boolean playNote(Note n)` e `public void playWrongNote()`. Entrambi questi metodi vengono scatenati dai `listener` delle azioni compiute dall'utente, che riguardano la pressione di tasti sulla tastiera virtuale a schermo. In particolare `playNote`, dopo l'emissione del suono della nota, si preoccupa di confrontare se la nota premuta corrisponde alla nota corretta sullo spartito e successivamente passa la testina alla nota successiva; se é presente un cambio di livello gestisce la generazione di un nuovo sparito.

```java
public boolean playNote(Note n){
  ClefScore score = scoreList.get(targetScoreIndex);
  Note targetNote = score.getNote(targetNoteIndex);

  Runnable play = () -> {
    Play player = new Play();
    player.midi(new jm.music.data.Note(n.toMidi(), 0.5, 127), false);
  };
  new Thread(play).start();

  boolean hit = (targetNote.compareTo(n) % 12) == 0;
  if(hit) ...
    changes.firePropertyChange("hit", oldHit, this.hit);
	else ...
    changes.firePropertyChange("miss", oldMiss, this.miss);

  if(targetNoteIndex < scoreList.get(targetScoreIndex).size() - 1) {
    targetNoteIndex++;
    startTimer();
  }else{
    if(targetScoreIndex < scoreList.size() - 1) {
      targetScoreIndex++;
      targetNoteIndex = 0;
      startTimer();
    }else{
      timer.stop();
      triggerNewLevel();
    }
  }
  changes.firePropertyChange("updateScore", null, toString());
  return hit;
}
```

In modo simile, `wrongNote` viene scatenato quando scade il timer entro il quale si doveva premere una nota; a quel punto la nota é conteggiata come sbagliata e si passa alla successiva.

### View

Il componente _view_ é formato da 3 classi, `Window` che si occupa della finestra principale; `WhiteKey`, che estende `Key` e che a sua volta estende `JPanel`, e si occupa della visualizzazione di un tasto bianco nella tastiera virtuale; `BlackKey`, analoga a quella precedente ma che si preoccupa di visualizzare un tasto nero.

![View](static/game.png)

Il metodo princiapale della classe `Window`, oltre al costruttore che setta ed aggiunge al pannello tutti gli elementi presenti nella schermata, é `public void propertyChange(PropertyChangeEvent evt)`, che si occupa di gestire gli eventi che provengono dal model, catalogarli e attivare un diverso update della parte visuale, contestualmente al cambiamento del model. Si ricorda che la classe `Window` implementa un `PropertyChangeListener` appositamente per "ascoltare" i cambiamenti apportati nel model.

```java
@Override
public void propertyChange(PropertyChangeEvent evt) {
  switch(evt.getPropertyName()){
    case "newLevel":
      levelLabel.setText("LEVEL " + evt.getNewValue().toString());
      break;
    case "updateScore":
      updateScore((String) evt.getNewValue());
      counterLabel.setText("Time left: 10 sec.");
      break;
    case "hit":
      hitLabel.setText(evt.getNewValue().toString());
      break;
    case "miss":
      missLabel.setText(evt.getNewValue().toString());
      break;
    case "complete":
      JOptionPane.showMessageDialog(this, evt.getNewValue(), "Level complete", 1);
      break;
    case "timeChange":
      counterLabel.setText("Time left: " + evt.getNewValue() + " sec.");
      break;
    case "timeUp":
      updateStatus("Too slow!");
      break;
  }
}
```



### Controller

Il controller si esaurisce con la classe `Handler`. La classe `Handler` contiene due riferimenti ad oggetti di tipo _model_ e _view_, in modo che il _controller_ abbia modo di invocare i metodi pubblici di entrambe le classi. La classe, oltre a contenere i listener che andrá ad aggiungere ai componenti interagibili del programma, contiene anche metodi per gestire tutta la _chain-invocation_ della pressione di un tasto sulla tastiera. Di seguito un'esempio.

```java
private void presedNote(Pitch p, Accidental a, int keyListCode, int index) {
  Note playedNote = null;
  try {
    playedNote = new Note(p, 4, a); // Stub octave = 4
    model.playNote(playedNote); // Propaga la pressione sul model
    view.pressedKey(keyListCode, index); // propaga la pressione sulla vista
  } catch (NoteException e) {
    e.printStackTrace();
  }
}
```

### Test

Tramite la suite JUnit sono stati implementati una serie di test, per controllare l'effettivo funzionamento dei metodi legati alla parte di gestione musicale delle note e della loro manipolazione. Sono riportati due test di esempio.

```java
@Test
public void testCompare2(){
	Note n1 = null;
  Note n2 = null;
  try {
  	n1 = new Note(Pitch.C, 4, Accidental.NATURAL);
    n2 = new Note(Pitch.C, 4, Accidental.SHARP);
  } catch (NoteException e) {
  	fail(e.getMessage());
  }
	assertEquals(-1, n1.compareTo(n2));
}

@Test
public void testCompare3(){
	Note n1 = null;
	Note n2 = null;
	try {
		n1 = new Note(Pitch.C, 5, Accidental.NATURAL);
		n2 = new Note(Pitch.C, 4, Accidental.NATURAL);
	} catch (NoteException e) {
		fail(e.getMessage());
	}
	assertEquals(12, n1.compareTo(n2));
}
```

Per velocizzare il processo di test con maggiori casistiche rispetto a quelle inserite manualmente si é deciso di procedere tramite il framework _jwik_. Grazie ad esso é possibile indicare una proprietá che si vuole mantenere vera e generare automaticamente un numero molto grande di casi di test aderenti alle regole indicate, e controllare se verificano la proprietá. Segue un esempio pratico.

```java
@Report({Reporting.GENERATED, Reporting.FALSIFIED})
@Property(tries = 10000, seed = "12345")
public void testPBSemitonesFromBase(@ForAll("validNote") Integer[] noteValues){
  Pitch pitch = Pitch.values()[noteValues[0]];
  int octave = noteValues[1];
  Accidental accidental = Accidental.values()[noteValues[2]];

  try {
    Note note = new Note(pitch, octave, accidental);
    assertEquals(octave * 12 + pitch.getSemitones() + accidental.getSemitones(), note.semitonesFromBase());
  } catch (NoteException e) {
    fail(e.getMessage());
  }
}
```



## Osservazioni
### Font

Per la riproduzione del pentagramma, delle chiavi musicali e delle note con relative alterazioni é stato usato un font gratuito creato ad-hoc, il suo nome é StaffClefPitchesEasy ed é reperibile al seguente [link](static/STAFCPE_.TTF) presente all'interno del repository. Il font comprende una serie di note giá posizionate sul rigo con varie alterazioni. 

### JMusic

Per rendere il serious game piú appetibile si é deciso di aggiungere un feedback sonoro alla pressione o al click delle note sulla tastiera virtuale. É stata scelta la libreria _JMusic_ di _Explodingart_, configurata nel `build.gradle` e reperita direttamente da _gradle_ in fase di pre-compilazione dai repository _maven_. L'uso della libreria é particolarmente limitato in questo software e si conclude riproducendo il suono richiesto alla pressione dei tasti. 

Una considerazione da riportare é il fatto che non é chiaro dove si collochi la riproduzione di un suono all'interno del modello MVC; in questo caso si é scelto per comoditá di porre il frammento di codice adibito alla riproduzione del suono all'interno del model.

## Sviluppi futuri

* In ottica serious game un possibile miglioramento sarebbe l'aggiunta di un resoconto al termine di ogni livello, in modo che l'utente possa visualizzare delle statistiche che possano aiutarlo a riconoscere un miglioramento.
* É possibile rivedere l'interfaccia grafica in modo da renderla più coinvolgente ed invogliare l'utente nell'apprendimento.
* Si potrebbe gestire in modo migliore la nota da suonare, gestendo un oggetto che fluttua sotto la nota senza evidenziare lo sfondo in modo invasivo; questo peró prevederebbe un refactoring molto grosso nel modo in cui viene presentato all'utente lo spartito.
* Sarebbe possibile segnare con un colore rosso le note giá suonate sbagliate, e in ottica serious game riportare sotto di esse, al termine del livello, la nota suonata e la nota da suonare.
* Per rendere il software piú significativo sarebbe possibile aggiungere le chiavi mancanti al font con le reletive mappature; in questo caso saranno generate tutte le possibili combinazioni di una partitura di setticlavio.